package io.ionic.starter;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.getcapacitor.BridgeActivity;
import com.getcapacitor.Plugin;
import com.gspl.leegalitysdk.Leegality;

import java.util.ArrayList;

public class MainActivity extends BridgeActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);


    Intent intent = new Intent(getApplicationContext(), Leegality.class);
    intent.putExtra("url", "https://ABC.sandbox.leegality.com/sign/b0b0462a-4b28-4906-922d-2bcca647e1c1");
    startActivityForResult(intent, 121);
    // Initializes the Bridge
    this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
      // Additional plugins you've installed go here
      // Ex: add(TotallyAwesomePlugin.class);
    }});
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    //Check requestCode, If it match with the provided REQUEST_CODE then this response is for the signing process.
    if (requestCode == 121) {
      String error = data.hasExtra("error") ? data.getExtras().getString("error") : null;
      String message = data.hasExtra("message") ? data.getExtras().getString("message") : null;
      if (error != null) {
        Toast.makeText(this, "Error: " + error, Toast.LENGTH_LONG).show();
      } else if (message != null) {
        Toast.makeText(this, "Message: " + message, Toast.LENGTH_LONG).show();
      }
    }
    super.onActivityResult(requestCode, resultCode, data);
  }
}
